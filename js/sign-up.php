<?php

include('includes/config.inc.php');
include('includes/functions.inc.php');

$page_title = 'Sign-up';


    $db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME )
        or die( mysqli_connect_error() );

    if( $_POST[ 'action' ] == 'sign-up'){
            $errors = sign_up_new_user($db,
                              $_POST[ 'firstname' ],
                              $_POST[ 'lastname' ],
                              $_POST[ 'email' ],
                              $_POST[ 'password' ],
                              $_POST[ 'streetaddress' ],
                              $_POST[ 'city' ],
                              $_POST[ 'province' ],
                              $_POST[ 'postalcode' ],
                              $_POST[ 'phonenumber' ],
                              $_POST[ 'type' ],
                              $_POST[ 'storename' ],
                              $_POST[ 'storedescription' ] );
                                                 

    } else {
        echo 'Nothing Submitted Yet.';
    }

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Closet</title>
	
	
	
	 <!-- BOOTSTRAP STYLE LINK -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	
	 <!-- JQUERY LINK -->
    <script src="js/jquery-1.12.4.min.js"></script>
	
    <link rel="stylesheet" href="css/styles.css" />
    
  
    <script>
    
        $(function() {
            
            //HIDE STORE FORM ON LOAD
            $("#store-form").hide();
            
            //CLICK BUSINESS RADIO BUTTON TO SHOW STORE FORM
            $("#business").click(function() {
                $("#store-form").show();
            });//business radio button
            
            //CLICK INDIVIDUAL RADIO BUTTON TO HIDE STORE FORM
            $("#individual").click(function() {
                $("#store-form").hide();
            });//individual radio button
            
        });//docready
    
    </script>

</head>

<body>

     <!--   NAV-->
    <?php include('includes/templates/components/nav.tpl.php'); ?>

	<section id="forms-section" class="container">

		<form id="signup" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">

			<input type="hidden" name="action" value="sign-up" />

			<div class="col33">

				<legend>Sign Up</legend>

			</div>	
			
<!--  SELECT USER TYPE -->
			
			<div class="form-row">

				<div class="form-group col-md-6">

					<input class="form-check-input" type="radio" id="individual" name="type" value="0" checked>

					<label class="form-check-label" for="individual">Individual</label>

				</div>

				<div class="form-group col-md-6">

					<input class="form-check-input"  type="radio" id="business" name="type" value="1">

					<label class="form-check-label" for="business">Business</label>

				</div>
				
			</div>	

<!--  INPUT NAME AND LASTNAME-->
				
			<div class="form-row">
	
			
 				<div class="form-group col-md-6">
					
					<?php echo $errors[ 'first_name' ]; ?>
					
					<input class="form-control" id="first-name" placeholder="First name" name="firstname" type="text" size="80" value="<?php echo $_POST[ 'firstname' ]; ?>" />
					
				</div>	
					
				<div class="form-group col-md-6">
					
					
					<?php echo $errors[ 'last_name' ]; ?>
					
					<input class="form-control" id="last-name" placeholder="Last name" name="lastname" type="text" size="80" value="<?php echo $_POST[ 'lastname' ]; ?>" />
					
				</div>
				
					
			</div>
			
<!--  INPUT EMAIL AND PASSWORD-->
			
			<div class="form-row">		

					<div class="form-group col-md-6">
						
						<label for="inputEmail4">Email</label>
						<?php echo $errors[ 'email' ]; ?>
						
						<input class="form-control" placeholder="john.doe@example.com" id="inputEmail4" name="email" type="email" size="80" value="<?php echo $_POST[ 'email' ]; ?>" />
						
					</div>
				
					<div class="form-group col-md-6">
						
						<label for="inputPassword4">Password</label>
						<?php echo $errors[ 'password' ]; ?>
						
						<input class="form-control" id="inputPassword4" placeholder="Enter your password." type="password" name="password" size="80" />
					</div>
				
			</div>
			
			<div class="col33">

				<legend>Delivery</legend>

			</div>
				
				<div class="form-group">
					
					<label for="inputAddress">Address</label>
					<?php echo $errors[ 'street_address' ]; ?>
					
					<input placeholder="ex. 123 All Street, Unit 1" name="streetaddress" type="text" size="50" value="<?php echo $_POST[ 'streetaddress' ]; ?>" />
					
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						
						<label for="inputCity">City</label>
						<?php echo $errors[ 'city' ]; ?>
						
						<input class="form-control" id="inputCity" placeholder="ex. Toronto" name="city" type="text" size="80" value="<?php echo $_POST[ 'city' ]; ?>" />
						
					</div>
					
				 <div class="form-group col-md-4">

						<label for="inputState">Province</label>
						<?php echo $errors[ 'province' ]; ?>

						<!-- select: creates a drop-down menu -->
						<select id="inputState" class="form-control" name="province">

							<!-- optgroup: allows categorization of options, with labels. -->
							<optgroup label="Provinces">

								<!-- option: individual options, values are submitted -->
								<option value="AB">Alberta</option>
								<option value="BC">British Columbia</option>
								<option value="MB">Manitoba</option>
								<option value="NB">New Brunswick</option>
								<option value="NL">Newfoundland and Labrador</option>
								<option value="NS">Nova Scotia</option>
								<option selected value="ON">Ontario</option>
								<option value="PE">Prince Edward Island</option>
								<option value="QC">Quebec</option>
								<option value="SK">Saskatchewan</option>
								<option value="NT">Northwest Territories</option>
								<option value="NU">Nunavut</option>
								<option value="YT">Yukon</option>
								
							</optgroup>

						</select>

					</div> 
					 

					<div class="form-group col-md-2">
						
						<label for="inputZip">Postal Code</label>
						<?php echo $errors[ 'postal_code' ]; ?>
						
						<input class="form-control" id="inputZip" name="postalcode" type="text" size="6" value="<?php echo $_POST[ 'postalcode' ]; ?>" />
						
					</div>

					<li>
						<label>Phone Number</label>
						<?php echo $errors[ 'phone_number' ]; ?>
						<input placeholder="1234567890" name="phonenumber" type="number" size="10" value="<?php echo $_POST[ 'phonenumber' ]; ?>" />
					</li>
				</ol>
		

			<!-- STORE FORM-->
			<fieldset id="store-form">

				<ul>

					<li><input type="text"
							   id="store-name"
							   name="storename"
							   placeholder="Store Name"
							   value="<?php echo $_POST[ 'storename' ]; ?>"></li>

					<li><input type="text"
							   id="store-description"
							   name="storedescription"
							   placeholder="Store Description"
							   value="<?php echo $_POST[ 'storedescription' ]; ?>"></li>


				</ul>

			</fieldset>


			<fieldset class="col33">
				<ol>
					<li>
						<input type="submit" value="Submit" />
					</li>
				</ol>
			</fieldset>

		</form>

	</section>

       

     <?php include('includes/templates/components/footer.tpl.php'); ?>

</body>

</html>
