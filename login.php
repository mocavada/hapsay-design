<?php
    // start the PHP session
    session_start();

    // load configuration settings
    require( 'includes/config.inc.php' );

    // load helpful functions into memory
    require( 'includes/functions.inc.php' );

    // determine which pagse is being viewed
    $page = DEFAULT_PAGE;
    if( isset( $_GET[ 'page' ] ) ){
        $page = $_GET[ 'page' ];
    }

    // check login status
    if( !is_public_page( $page, $public_pages ) ){  
        check_login();
    }

    // connect to mysql server
    $db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME )
        or die( mysqli_connect_error() );


    if( $_POST[ 'action' ] =='login') {
        $errors = log_user_in( $db, $_POST[ 'email' ], $_POST[ 'password' ] );
    }

    
?>
<!-- HEAD-->
 <?php include('includes/templates/components/head.tpl.php'); ?>
<!-- NAV-->
<?php include('includes/templates/components/nav.tpl.php'); ?>

       <main>
        <section id="login-page">
            <h2>My Account</h2>
            <fieldset>
                <legend>Login</legend>
                <form id="login" action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
                    <input type="hidden" name="action" value="login" />
                    <ol>
                        <li>
                            <label for="email">Email&#42;</label>
                            <?php echo $errors[ 'email' ]; ?>
                                <input placeholder="Enter your email address." type="text" name="email" size="80" value="<?php echo $_POST[ 'email' ]; ?>" />
                        </li>

                        <li>
                            <label>Password&#42;</label>
                            <?php echo $errors[ 'password' ]; ?>
                                <input placeholder="Enter your password." type="password" name="password" size="80" />
                        </li>

                        <li>
                            <button>
                                <span>Log In</span>
                            </button>
                        </li>
                    </ol>
                </form>
            </fieldset>
        </section>
        </main>
    </body>

</html>
