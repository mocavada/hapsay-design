<?php
//QUERY REFERENCE FOR JOINING TABLE ->
//$query = "SELECT * FROM design
//		 JOIN store WHERE design.store_id = store.store_id 
//		 AND store.login_id = '" . $_SESSION['login_id'] . "' LIMIT 1";

//JOINING TWO TABLE ->
//$query = "SELECT * 
//		  FROM design AS de INNER JOIN store AS st
//		  ON de.store_id = st.store_id";
		
//JOINING THREE TABLE ->
//$query = "SELECT * 
//		  FROM design AS de INNER JOIN store AS st
//		  ON de.store_id = st.store_id
//		  INNER JOIN product As pr
//		  ON de.design_id = pr.design_id";

//JOINING FOUR TABLE ->
//$query = "SELECT * 
//		  FROM design AS de INNER JOIN store AS st
//		  ON de.store_id = st.store_id
//		  INNER JOIN product As pr
//		  ON de.design_id = pr.design_id
//		  INNER JOIN login AS lo
//		  ON lo.login_id = st.login_id";

//JOINING FOUR TABLE to $_SESSION[ 'login_id' ]
//$query = "SELECT * 
//		  FROM design AS de INNER JOIN store AS st
//		  ON de.store_id = st.store_id
//		  INNER JOIN product As pr
//		  ON de.design_id = pr.design_id
//		  INNER JOIN login AS lo
//		  ON lo.login_id = '" . $_SESSION['login_id'] . "' LIMIT 1";

//$query = "SELECT * 
//			FROM design AS de
//			JOIN store AS st
//			ON de.store_id = st.store_id
//			JOIN login AS lo
//			ON lo.login_id = '" . $_SESSION['login_id'] . "' LIMIT 1";


//CLOSET PRODUCT GALLERY DISPLAY

	echo "<ul id='product'>" .
			"<li><a href='product.php?id=" . $row[ 'product_id' ] . "'><img src='" . PRODUCT_FOLDER . $row[ 'file_name' ] . "' alt='" . $row[ 'name' ] . "' /></a></li>" .
			"<li>Name: " . $row[ 'name' ] . "</li>" .
			"<li>Description: " . $row[ 'description' ] . "</li>" .
			"<li>Price: $" . $row[ 'price' ] . "</li>" .
		 "</ul>";









    session_start();

    require( 'includes/config.inc.php' );
    require( 'includes/functions.inc.php' );

	$db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME )
        or die( mysqli_connect_error() );

	// CONNECT TO UPLOADER FUNCTION
	include( 'includes/product-uploader.inc.php' );
	include( 'includes/store-uploader.inc.php' );

    switch( $_SERVER[ 'REQUEST_METHOD' ] ){
        case 'GET':
            switch( $_GET[ 'action' ] ){
                case 'delete':
                    delete_task( $db, $_GET[ 'delete_id' ] );
                break;
                default:
                    
                break;
            }
        break;
        default:
            // unsupported request method, this is an error condition
        break;
    }

    //UPLOAD PATH VARIABLES
	$design_folder  = SITE_ROOT . SMALL_FOLDER ;
	$store_folder  = SITE_ROOT . STORE_MEDIUM_FOLDER ;


    // QUERY FOR STORE TO UPLOAD BANNER
	$query = "SELECT * FROM login JOIN store WHERE login.login_id = store.login_id AND login.login_id = '".$_SESSION['login_id']."'";

    $result = mysqli_query( $db, $query ) or die( mysqli_error( $db ).'<br>'. $query );
    
    $store_row = mysqli_fetch_assoc($result);
    //POST USER TYPE
    $user_type = $store_row[ 'type' ]; 
    
    
	// QUERY TO SHOW PRODUCTS DISPLAY IN OWN STORE
	$query = "SELECT * FROM product, store, login WHERE login.login_id = store.login_id AND product.store_id = store.store_id AND login.login_id = '".$_SESSION['login_id']."' ";

	$result = mysqli_query( $db, $query ) or die( mysqli_error( $db ).'<br>'. $query );
    
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Upload - Gallery Application</title>

	<!-- link to the main stylesheet -->
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/responsive.css" />

	<!--[if lt IE 9]>
	<script src="js/html5shiv.min.js"></script>
	<![endif]-->
</head>
<body>
      
<?php include('includes/templates/components/nav.tpl.php'); ?>
	
	<div class="spacer col33"></div>
	
	<div class="col33">
		SESSION LOGin ID = <?php echo $_SESSION[ 'login_id' ]; ?>
		SESSION EMAIL = <?php echo $_SESSION[ 'email' ]; ?>
		TYPE = <?php echo $user_type; ?>
	</div>	
	

	<section class="mystore col23 fl">		
		<div class="store-display col33">
			<ul>
				<li>STORE ID = <?php 
					if ($store_row[ 'login_id' ] == $_SESSION[ 'login_id' ]) {
					echo $store_row[ 'store_id' ]; 
					
					} else {
						
					echo 'UPLPOAD A BANNER' ;	
					}
					
					?></li>
				<li>STORE NAME = <?php echo $store_row[ 'store_name' ]; ?></li>
				
				<li>PROMOTION = <?php echo $store_row[ 'promotion' ]; ?></li>
				<li>
				<img src="<?php 
				if( isset( $store_folder ) ){
					echo $store_folder . $store_row[ 'banner_filename' ];
				} ?>" alt="<?php echo $store_row[ 'store_name' ]; ?>" />
				</li>
			</ul>
		</div>	
		
<?php while( $row = mysqli_fetch_assoc($result) ): ?>
			
		<div class="design-display col12">
				<ul>
					
				<li><img src="<?php 
				if( isset( $design_folder ) ){
					echo $design_folder . $row[ 'design_filename' ];
				} ?>" alt="<?php echo $row[ 'design_name' ]; ?>" /></li>	
				
				<li>DESIGN ID = <?php echo $row[ 'design_id' ]; ?></li>
					
				<li>DESIGN NAME = <?php echo $row[ 'design_name' ]; ?></li>
					
				<li>DESIGN DESCRIPTION = <?php echo $row[ 'design_description' ]; ?></li>
					
				<li>STORE ID = <?php echo $row[ 'store_id' ]; ?></li>
					
				<?php if( $row[ 'type' ] ==  1 ): ?>	
				<li><a class="genericons-neue genericons-neue-close-alt" 
				href="?action=delete&amp;delete_id=<?php echo $row[ 'design_id' ]; ?>">
				<span>Delete</span>
				</a></li>	
				<?php endif; ?>	
				
				</ul>
		</div>	
		
<?php endwhile; ?>
		
		
	</section>
	
<!--   UPLOADER / MY CART CONTAINER -->	
	<section class="uploader col13 fr">
		
<?php if($user_type == 1): ?>
		<div class="col33 upload-store">

		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" 
			  method="post"
			  enctype="multipart/form-data">
			
			<?php echo $errors[ 'storeupload' ]; ?>
			
			<input type="hidden" name="store-upload-started" value="true" />
			
			<ol>

				<li>
					<label>Store Status</label>
					<?php echo $errors[ 'promotion' ]; ?>
					<textarea name="promotion" rows="3" cols="80"><?php 
						echo $_POST[ 'promotion' ]; ?></textarea>
				</li>
				<li>
					<label>Change Banner</label>
					<input type="file" name="store-user-upload" />
				</li>
				<li>
					<input type="submit" value="Upload" />
				</li>
			</ol>
		</form>

		</div>
		
		<div class="col33 upload-design">

		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" 
			  method="post"
			  enctype="multipart/form-data">
			<?php echo $errors[ 'upload' ]; ?>
			<input type="hidden" name="upload-started" value="true" />
			<ol>
				<li>
					<label>Design Name</label>
					<?php echo $errors[ 'design_name' ]; ?>
					<input type="text" name="design_name" size="80" 
						   value="<?php echo $_POST[ 'design_name' ]; ?>" />
				</li>

				<li>
					<label>Design Description</label>
					<textarea name="design_description" rows="3" cols="80"><?php 
						echo $_POST[ 'design_description' ]; ?></textarea>
				</li>
				<li>
					<input type="file" name="user-upload" />
				</li>
				<li>
					<input type="submit" value="Upload" />
				</li>
			</ol>
		</form>
	</div>	
<?php else: ?>	
		<div><h2>MY CART</div>	
		
<?php endif; ?>			

	</section> 
	
	<div class="clear"></div>
<!--   FOOTER-->
<?php //include('includes/templates/components/footer.tpl.php'); ?>             
</body>
</html>
