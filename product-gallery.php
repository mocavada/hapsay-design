<?php
session_start();

include('includes/config.inc.php');
include('includes/functions.inc.php');

$db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME )
        or die( mysqli_connect_error() );

$query = "SELECT * FROM product";

$result = mysqli_query( $db, $query ) or die( mysqli_error( $db ).'<br>'. $query );

?>

<!-- HEAD-->
 <?php include('includes/templates/components/head.tpl.php'); ?>
<!--   NAV-->
<?php include('includes/templates/components/nav.tpl.php'); ?>
    
    <!--   GALLERY CONTAINER -->
<div class="contaier">

	<h3>PRODUCT GALLERY</h3>
	<h4>Different PRODUCT Details</h4> 
	
	<section class="row">

	<?php

		while( $row = mysqli_fetch_assoc($result) ) {

		echo "<div class='col-lg-3 col-sm-6 col-xs'>" .
		
			"<a href='product.php?id=" . $row[ 'product_id' ] . "'>
				<img class='img-fluid' src='" . PRODUCT_FOLDER . $row[ 'file_name' ] . "' alt='" . $row[ 'name' ] . "' />
			</a>" .

			"</div>";
		}
	?>

	</section>

</div>
    
<?php include('includes/templates/components/footer.tpl.php'); ?>

</body>
</html>