<?php
session_start();

include('includes/config.inc.php');
include('includes/functions.inc.php');


$db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME )
        or die( mysqli_connect_error() );


$query = "SELECT * FROM orders
            JOIN product WHERE orders.product_id = product.product_id
            AND orders.login_id = " . $_SESSION['login_id'];


$result = mysqli_query( $db, $query ) or die( mysqli_error( $db ).'<br>'. $query );

switch( $_SERVER[ 'REQUEST_METHOD' ] ){
			
        case 'GET':
			
            switch( $_GET[ 'action' ] ){
                case 'delete':
                    delete_order( $db, $_GET[ 'delete_id' ] );
                break;
					
                default:
                    
                break;
            }
        break;
        default:
            // unsupported request method, this is an error condition
        break;
    }


?>
<!-- HEAD-->
 <?php include('includes/templates/components/head.tpl.php'); ?>
<!--   NAV-->	
<?php include('includes/templates/components/nav.tpl.php'); ?>
	
<!--   CART CONTAINER-->	
<section class="col33 cart-container">

	<h2>My Cart</h2>
	
	<ul id="products">
	    
    <?php while( $row = mysqli_fetch_assoc($result) ) {

        echo "<ul id='product'>" .
                "<li><img src='" . PRODUCT_FOLDER . $row[ 'file_name' ] . "' alt='" . $row[ 'name' ] . "' /></li>" .
                "<li>Name: " . $row[ 'name' ] . "</li>" .
                "<li>Description: " . $row[ 'description' ] . "</li>" .
                "<li>Size: " . $row[ 'size' ] . "</li>" .
                "<li>Price: $" . $row[ 'price' ] . "</li>" .
			
				"<li>
				<a class=\"genericons-neue genericons-neue-close-alt\" 
				
				href=\"?action=delete&amp;delete_id={$row[ 'order_id' ]}&amp;id={$_SESSION[ 'login_id' ]}\" >
				
                <span>Delete</span>
                </a>
                </li></ul>";

    } ?>
	    
	</ul>

			
</section>
<!--   END CART CONTAINER-->	
	
<!--   FOOTER-->
<?php include('includes/templates/components/footer.tpl.php'); ?>
</body>
</html>

