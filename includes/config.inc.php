<?php

    /*** PHP Error Settings*/

    // show all errors, but not the notices
    error_reporting( E_ALL & ~E_NOTICE );

    // show errors in the browser
    ini_set( 'display_errors', 1 );

    /*** Timezone Settings*/

    date_default_timezone_set( 'America/Toronto' );

    /*** Site Settings*/

    define( 'SITE_TITLE',   'Hapsay' );
    define( 'DEFAULT_PAGE', 'index' );
    define( 'URL_REWRITE',  true );
    define( 'SITE_ROOT',    'http://hapsay.com/' );
	define( 'UPLOAD_ROOT',    'uploads/' );


    /*** File Upload Settings*/
	
    define( 'PRODUCT_FOLDER', UPLOAD_ROOT . 'products/' );
    define( 'BANNER_FOLDER', UPLOAD_ROOT . 'banners/' );
    define( 'PROMOTION_FOLDER', UPLOAD_ROOT . 'promotions/' );
    define( 'PREVIEW_FOLDER', PRODUCT_FOLDER . 'preview/' );

	//	Upload Check Settings
    define( 'ALLOWED_FILE_TYPES', 'image/png,image/gif,image/jpeg,image/pjpeg' );
	define( 'RANDOMIZE_FILENAMES' , 'true' );

    /*** File Extensions*/

	$mime_to_extension = array (
		'image/jpeg' => '.jpg',
		'image/pjpeg' => '.jpg',
		'image/gif' => '.gif',
		'image/png' => '.png'
	);

    /*** Database Credentials (change these to your own)*/
	define( 'DB_HOST',      'localhost' );
    define( 'DB_USER',      'hapsayco_moc' );
    define( 'DB_PASSWORD',  'M0c78922' );
    define( 'DB_NAME',      'hapsayco_hapsay' );

    /*** Login Settings*/

    // only value that indicates a logged in user
    define( 'LOGIN_TOKEN', 'N1cOsWvvezifyTfLuPE6ABCdPdJFp8RvLV1Qg4qr' );

    // pages that don't require a login
    $public_pages = array( '404', 'login', 'about', 'index', 'stores', 'sign-up', 'gallery' );

	session_start();

//	$_SESSION['cart'] = array();
	