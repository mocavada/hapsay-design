<?php

$errors = array();
    if( isset( $_POST[ 'upload-started' ] ) ){
        // form has been submitted
        
        if( strlen( $_POST[ 'name' ] ) < 1 ){
            $errors[ 'name' ] = '<p class="error">Please enter a title for the image.</p>';
        } else {
            if( $_FILES[ 'user-upload' ][ 'error' ] == UPLOAD_ERR_OK ){
                // the file uploaded successfully to the temp folder

                // current location of the file (temporary folder and filename)
                $temp_location = $_FILES[ 'user-upload' ][ 'tmp_name' ];

                // retrieve image-specific information about the uploaded file
                // Note: if you want to upload non-images, this needs to be
                // changed to mime_content_type()
                $info = getimagesize( $temp_location );

                if( strpos( ALLOWED_FILE_TYPES, $info[ 'mime' ] ) !== false ){
                // the file's MIME type matched one of the types in our
                // ALLOWED_FILE_TYPES constant

                    if( RANDOMIZE_FILENAMES ){
                        // generate a filename, based on time and original filename
                        // use a hash to get a filename-safe string
                        $product_filename 
                            = sha1( microtime() . $_FILES[ 'user-upload' ][ 'name' ] );
                        // get the correct extension for the file, from MIME type
                        $extension = $mime_to_extension[ $info[ 'mime' ] ];
                        // assemble into a complete file path to the new file
                        $destination = UPLOADS_PRODUCT_FOLDER . $product_filename . $extension;
                    } else {
                        // new folder combined with original filename
                        $destination = UPLOADS_PRODUCT_FOLDER . $_FILES[ 'user-upload' ][ 'name' ];
                    }

                    // attempt to move the file from the temp folder to the
                    // new file path that was generated above
                    if( move_uploaded_file( $temp_location, $destination ) ){

//						????Questions
						
                        $query = "SELECT * FROM store 
                                JOIN login WHERE store.login_id = login.login_id
                                AND login.email = '" . $_SESSION[ 'email' ] . "' LIMIT 1";

                        $result = mysqli_query( $db, $query ) or die( mysqli_error( $db ).'<br>'. $query );

                        $row = mysqli_fetch_assoc( $result );
                        
                        $store_id = $row['store_id'];
						
                        
                        $small  = resize_to_fit( $destination, 150, SMALL_FOLDER );
                        $medium = resize_to_fit( $destination, 500, MEDIUM_FOLDER );
                        $large  = resize_to_fit( $destination, 940, LARGE_FOLDER );

                        // file was moved successfully
                        $preview = 
                            "<h2>Your image was Successfully Uploaded.</h2>
                             <a href=\"$destination\">
                                <img src=\"$medium\" alt=\"medium preview\" />
                             </a>";
                        
                        // gather the needed info
                        $product_name          = $_POST[ 'name' ];
                        $product_description    = $_POST[ 'description' ];
                        $file_name       = str_replace( UPLOADS_PRODUCT_FOLDER , 
                                                       '', 
                                                       $destination );
                        
                        // sanitize the info
                        $product_name       = sanitize( $db, $product_name );
                        $product_description = sanitize( $db, $product_description );
                        $file_name    = sanitize( $db, $file_name );
                        
                        $query = 	"INSERT INTO product (store_id,name,description,file_name )
						
									VALUES
						('$store_id','$product_name','$product_description','$file_name')";
                        
                        $result = mysqli_query( $db, $query )
                            or die( mysqli_error( $db ) 
                                    . '<br>' 
                                    . $query ) ;
                        
                        $_POST[ 'name' ]       = '';
                        $_POST[ 'description' ] = '';
                        
                    } else {
                        // likely causes:
                        // - missing uploads folder
                        // - permissions of folder are wrong
                        // - destination filename is garbled up somehow
                        $errors[ 'upload' ] = '<p class="error">
                            There was a problem saving the file;
                            please contact the administrator.
                        </p>';
                    }

                } else {

                    $errors[ 'upload' ] = '<p class="error">
                            The file uploaded is not one of the allowed
                            file types: gif, jpeg or png.
                        </p>';
                }

            } else {

                // there was some kind of server error

                switch( $_FILES[ 'user-upload' ][ 'error' ] ){
                    case UPLOAD_ERR_INI_SIZE:
                        $errors[ 'upload' ] = '<p class="error">
                            The uploaded file exceeds the
                            maximum allowed file size of ' 
                            . ini_get( 'upload_max_filesize' ) 
                            . ' </p>';
                    break;
                    case UPLOAD_ERR_NO_FILE:
                        $errors[ 'upload' ] = '<p class="error">
                            Please select a file to upload.</p>';
                    break;
                    case UPLOAD_ERR_CANT_WRITE:
                        $errors[ 'upload' ] = '<p class="error">
                            The server was unable to save the uploaded file.</p>';
                    break;
                    case UPLOAD_ERR_PARTIAL:
                        $errors[ 'upload' ] = '<p class="error">
                            The file was only partially uploaded.</p>';
                    break;
                    default:
                        $errors[ 'upload' ] = '<p class="error">
                            There was a server issue in uploading the file.</p>';
                    break;
                }
            }
        }
    }