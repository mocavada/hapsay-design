<?php

$errors = array();

    if( isset( $_POST[ 'store-upload-started' ] ) ){
        // form has been submitted
        
        if( strlen( $_POST[ 'promotion' ] ) < 1 ){
			
            //IF PROMOTION IS EMPTY - ERROR
            $errors[ 'promotion' ] = '<p class="error">Please enter Promotion.</p>';
			
        } else {
            
            
            
            if( $_FILES[ 'banner' ][ 'error' ] == UPLOAD_ERR_OK ){
                // the file uploaded successfully to the temp folder
                
                
                $temp_location = $_FILES[ 'banner' ][ 'tmp_name' ];
              
                $info = getimagesize( $temp_location );
                
                
                
                
                

                if( strpos( ALLOWED_FILE_TYPES, $info[ 'mime' ] ) !== false ){
                    

                    if( RANDOMIZE_FILENAMES ){
                        
                        $banner_filename = sha1( microtime() . $_FILES[ 'banner' ][ 'name' ] );
                        
                        // get the correct extension for the file, from MIME type
                        $extension = $mime_to_extension[ $info[ 'mime' ] ];
                        
                        // assemble into a complete file path to the new file
                        $destination = BANNER_FOLDER . $banner_filename . $extension;
                        
                        
                    } else {
                        
                        // new folder combined with original filename
                        $destination = BANNER_FOLDER . $_FILES[ 'banner' ][ 'name' ];
                        
                    }
                    
                    
                    
                    
                    

                    if( move_uploaded_file( $temp_location, $destination ) ){

                        $preview = resize_to_fit( $destination, 300, PREVIEW_FOLDER );

                        // file was moved successfully
                        $preview_message = 
                            "<h2>Your image was Successfully Uploaded.</h2>
                             <a href=\"$destination\">
                                <img src=\"$preview\" alt=\"Preview\" />
                             </a>";
                        
                        
                        //GET PROMOTION AND BANNER FILE FROM FORM
                        //FOR USE IN QUERY
						$promotion = $_POST[ 'promotion' ];
                        $banner_filename = str_replace( BANNER_FOLDER , '', $destination );
                        
                        
                        // sanitize the info
                        $promotion = sanitize( $db, $promotion );
                        $banner_filename  = sanitize( $db, $banner_filename );
                        
                        
                        //QUERY
                        $query = "UPDATE store SET
							banner_filename = '$banner_filename' ,
							promotion = '$promotion'
							WHERE login_id = {$_SESSION['login_id']}";
						
                        
                        $result = mysqli_query( $db, $query )
                            or die( mysqli_error( $db ) 
                                    . '<br>' 
                                    . $query ) ;
                        
                      
                        $_POST[ 'promotion' ] = '';
                        
                        
                        
                        
                        
                        
                    } else {
                        
                        $errors[ 'upload' ] = '<p class="error">
                            There was a problem saving the file;
                            please contact the administrator.
                        </p>';
                    }

                    
                    
                    
                    
                    
                } else {

                    $errors[ 'upload' ] = '<p class="error">
                            The file uploaded is not one of the allowed
                            file types: gif, jpeg or png.
                        </p>';
                }

            } else {

                // there was some kind of server error

                switch( $_FILES[ 'store-user-upload' ][ 'error' ] ){
                    case UPLOAD_ERR_INI_SIZE:
                        $errors[ 'storeupload' ] = '<p class="error">
                            The uploaded file exceeds the
                            maximum allowed file size of ' 
                            . ini_get( 'upload_max_filesize' ) 
                            . ' </p>';
                    break;
                    case UPLOAD_ERR_NO_FILE:
                        $errors[ 'storeupload' ] = '<p class="error">
                            Please select a file to upload.</p>';
                    break;
                    case UPLOAD_ERR_CANT_WRITE:
                        $errors[ 'storeupload' ] = '<p class="error">
                            The server was unable to save the uploaded file.</p>';
                    break;
                    case UPLOAD_ERR_PARTIAL:
                        $errors[ 'storeupload' ] = '<p class="error">
                            The file was only partially uploaded.</p>';
                    break;
                    default:
                        $errors[ 'storeupload' ] = '<p class="error">
                            There was a server issue in uploading the file.</p>';
                    break;
                }
            }
        }
    }
