<?php

    //PRODUCT GALLERY QUERY
    $product_query = "SELECT * FROM product
              JOIN store WHERE product.store_id = store.store_id
              AND store.store_id = $view_store_id";

    $product_result = mysqli_query( $db, $product_query )
        or die( mysqli_error( $db ).'<br>'. $product_query );

?>



<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Upload - Gallery Application</title>

    <!-- link to the main stylesheet -->
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />

    <!--[if lt IE 9]>
	<script src="js/html5shiv.min.js"></script>
	<![endif]-->
</head>

<body>

    <?php include('includes/templates/components/nav.tpl.php'); ?>

        <img src="<?php echo $store_folder . $store_row[ 'banner_filename' ]; ?>" alt="<?php echo $store_row[ 'store_name' ]; ?>" />


        <?php while( $product_row = mysqli_fetch_assoc($product_result) ): ?>

            <div class="design-display col12">

                <ul>

                    <li><img src="<?php echo $design_folder . $product_row[ 'file_name' ]; ?>" alt="<?php echo $product_row[ 'name' ]; ?>" /></li>

                    <li>Product ID =
                        <?php echo $product_row[ 'product_id' ]; ?>
                    </li>

                    <li>Product NAME =
                        <?php echo $product_row[ 'name' ]; ?>
                    </li>

                    <li>Product DESCRIPTION =
                        <?php echo $product_row[ 'description' ]; ?>
                    </li>

                    <li>STORE ID =
                        <?php echo $product_row[ 'store_id' ]; ?>
                    </li>

                    <?php if( $product_row[ 'type' ] ==  1 ): ?>
                        <li>
                            <a class="genericons-neue genericons-neue-close-alt" href="?action=delete&amp;delete_id=<?php echo $product_row[ 'product_id' ]; ?>">
                                <span>Delete</span>
                            </a>
                        </li>
                        <?php endif; ?>

                </ul>
            </div>

            <?php endwhile; ?>


                <div class="clear"></div>
                <!--   FOOTER-->
                <?php //include('includes/templates/components/footer.tpl.php'); ?>
</body>

</html>
