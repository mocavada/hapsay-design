

<!-- HEAD-->
<?php include('components/head.tpl.php'); ?>
<!-- NAV-->
<?php include('components/nav.tpl.php'); ?>

<main class="container">
	

	<section class="row">
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="3000">
			<div class="carousel-inner">
				<div class="carousel-item active">
				  <img class="d-block w-100" src="/images/slides/deep_vintage_toronto_by_cute_and_bright-d2x1kob.jpg" alt="First slide">
				</div>

				<div class="carousel-item">
				  <img class="d-block w-100" src="/images/slides/maleShirtColorfulWall.jpg" alt="Second slide">
				</div>

				<div class="carousel-item">
				  <img class="d-block w-100" src="/images/slides/maleShirtWhiteSquareWall.jpg" alt="Third slide">
				</div>
			</div>
			

			 <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
			</a>

			<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
			</a>

		</div>
	</section>	
	
	
	
	
	<section class="row">
		
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card">
			<img class="card-img-top" src="images/design/front.jpg" alt="Card image cap">
			<div class="card-body">
			<h5 class="card-title">Card title</h5>
			<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			</div>
			<ul class="list-group list-group-flush">
			<li class="list-group-item">Cras justo odio</li>
			<li class="list-group-item">Dapibus ac facilisis in</li>
			<li class="list-group-item">Vestibulum at eros</li>
			</ul>
			<div class="card-body">
			<a href="#" class="card-link">Card link</a>
			<a href="#" class="card-link">Another link</a>
			</div>
		</div>
		
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card">
			<img class="card-img-top" src="images/design/c.jpeg" alt="Card image cap">
			<div class="card-body">
			<h5 class="card-title">Card title</h5>
			<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			</div>
			<ul class="list-group list-group-flush">
			<li class="list-group-item">Cras justo odio</li>
			<li class="list-group-item">Dapibus ac facilisis in</li>
			<li class="list-group-item">Vestibulum at eros</li>
			</ul>
			<div class="card-body">
			<a href="#" class="card-link">Card link</a>
			<a href="#" class="card-link">Another link</a>
			</div>
		</div>
		
	</section>

	
	

</main>
<?php include('components/footer.tpl.php'); ?>

</body>

</html>
