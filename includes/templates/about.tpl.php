<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>About - <?php echo SITE_TITLE; ?></title>
        
        <!-- link to the main stylesheet -->
        <link rel="stylesheet" href="css/style.css" />
        
        <!--[if lt IE 9]>
	    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header>
            <h1><?php echo SITE_TITLE; ?></h1>
        </header>
        <main>
            <h2>About</h2>
            <p>This is a simple PHP/MySQL based application
               that helps you to keep track of tasks you need to do.</p>
        </main>
    </body>
</html>