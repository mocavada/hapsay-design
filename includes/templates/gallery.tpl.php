<?php

$query = "SELECT * FROM design ORDER By date_uploaded DESC";
$result = mysqli_query( $db, $query ) or die( mysqli_error( $db ).'<br>'. $query );

$counter = 0;

?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>CLOSET</title>
        
        <!-- link to the main stylesheet -->
        <link rel="stylesheet" href="/css/style.css" />
        
        <!--[if lt IE 9]>
	    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
    </head>
    <body>
       <header>
		   <h1>DESIGN GALLERY</h1>
		</header>
       <main>
	    
	   <section class="gallery col33 fl">
		<?php while( $row = mysqli_fetch_assoc($result) ): ?>
	   
		   <div class="box col14 fl">
				<a href="<?php echo UPLOADS_DESIGN_FOLDER . $row[ 'design_filename' ]; ?>">
				<img src="<?php echo MEDIUM_FOLDER                            . $row['design_filename']; ?>" 
                                 alt="<?php echo $row[ 'design_name' ];?>" />
				<p><?php echo $row[ 'design_name' ]; ?></p></a>	
		   </div> 
		   
		<?php $counter++; ?>
			
		<?php
			while( ($counter % 4) == 0 ) {
				echo "<div class=\"clear\"></div>";	
				break;
			}
		?>
		<?php endwhile; ?>
	   </section>
	   
	   </main>
        
    </body>
</html>