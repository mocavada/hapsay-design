<?php
    
    $_SESSION[ 'logged_in' ] = null;
    $_SESSION[ 'email' ]     = null;
    $_SESSION[ 'login_id' ]  = null;
    $_SESSION[ 'type' ]      = null;
    $_SESSION[ 'store_id' ]  = null;

    unset( $_SESSION[ 'logged_in' ] );
    unset( $_SESSION[ 'email' ] );
    unset( $_SESSION[ 'login_id' ] );
    unset( $_SESSION[ 'type' ] );
    unset( $_SESSION[ 'store_id' ] );

    redirect();