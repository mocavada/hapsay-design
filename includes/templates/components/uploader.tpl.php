<?php
if(basename($_SERVER['PHP_SELF']) == 'uploader.tpl.php'){
    die('ACCESS FORBIDDEN - 403');
} else {
?>

<!--   UPLOADER / MY CART CONTAINER -->


	<form action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" method="post" enctype="multipart/form-data">

	<?php echo $errors[ 'storeupload' ]; ?>

	<input type="hidden" name="store-upload-started" value="true" />


	<div class="row">
		<h5>Customize Store</h5>
	</div>	
	<div class="row">	

		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">Shout Out</span>	
			</div>	
			<textarea class="form-control" aria-label="With textarea"
				  name="promotion"
				  rows="3"
				  cols="80"><?php echo $_POST[ 'promotion' ]; ?>
			<?php echo $errors[ 'promotion' ]; ?></textarea>
		</div>
	</div>	
		
	<div class="row">	

	<h5>Change Banner</h5>

	<div class="input-group mb-3">
	<div class="input-group-prepend">
		<span class="input-group-text">Upload</span>
	</div>
	<div class="custom-file">
		<input class="custom-file-input" id="inputGroupFile01" type="file" name="banner" />
		<label class="custom-file-label" for="inputGroupFile01">Choose file</label>
	</div>
	</div>	
	</div>	
	<div class="row">

		<button type="submit" class="btn btn-primary" value="Upload">Submit</button>

	</div>		


	</form>







    <div class="col33 upload-design">

        <form action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" method="post" enctype="multipart/form-data">
            <?php echo $errors[ 'upload' ]; ?>
            <input type="hidden" name="upload-started" value="true" />
            <ol>
                <li>
                    <label>Product Name</label>
                    <?php echo $errors[ 'name' ]; ?>
                    <input type="text" name="name" size="80" value="<?php echo $_POST[ 'name' ]; ?>" />
                </li>

                <li>
                    <label>Product Description</label>
                    <textarea name="description" rows="3" cols="80">
                        <?php echo $_POST[ 'description' ]; ?>
                    </textarea>
                </li>
                <li>
                    <input type="file" name="user-upload" />
                </li>
                <li>
                    <input type="submit" value="Upload" />
                </li>
            </ol>
        </form>
    </div>

    
</section>
<?php
}
?>
        