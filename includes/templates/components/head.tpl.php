<!doctype html>
<html lang="en"><head>
	<!--GOOGLE ANALYTICS (gtag.js)-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107615923-1">
	</script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-107615923-1');
	</script>
	
	
	<!--MOBILE METAS-->
    <meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--SEO METAS-->
 	<meta name="description"  content="This is a T-Shirt Store Website">
    <meta name="keywords"  content="T-Shirt, Shirt Design, Design Store, T-Shirt Store, Hapsay">
	
	<!--FONT AWESOME-->
	<script defer src="/js/fontawesome-all.js"></script>
	<script defer src="/js/fa-v4-shims.min.js"></script>
	
	

	<!--SITE TTILE-->
    <title><?php echo SITE_TITLE; ?></title>
	
	<!-- FAVICON -->
	<link rel="icon" href="favicon.png">

	
	<!-- JQUERY LINK -->
	<script src="/js/jquery-1.12.4.min.js"></script>
	
	
	
	
	
	
	
	
    <!-- BOOTSTRAP STYLE LINK -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- CSS STYLE LINK -->
    <link rel="stylesheet" href="css/styles.css" />

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
</head>

<body>