
<header class="row">
	
		<ul class="nav nav-pills">

			<li class="nav-item">
				<a class="nav-link" href="<?php echo SITE_ROOT ?>"><?php echo SITE_TITLE; ?></a>
			</li>


			<li class="nav-item">
				<a class="nav-link" href="store-gallery.php">Stores</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="product-gallery.php">Products</a>
			</li>


			<?php if((!empty($_SESSION['logged_in'])) AND ($_SESSION[ 'type' ] == 1)): ?>

			<li class="nav-item">
				<a class="nav-link" href="<?php echo 'store.php?id=' . $_SESSION[ 'store_id' ]; ?>">My Store</a>

			</li>
			<li class="nav-item">
				<a class="nav-link" 
					  href="<?php echo ( URL_REWRITE ) ? 
							SITE_ROOT . 'logout' 
						  : SITE_ROOT . '?page=logout'; ?>">
						  Logout</a>
			</li>

			<?php elseif((!empty($_SESSION['logged_in'])) AND ($_SESSION[ 'type' ] == 0)): ?>				

			<li class="nav-item">
				<a class="nav-link" href="mycart.php">My Cart</a>
			</li>

			<li class="nav-item">
				<a class="nav-link"
					  href="<?php echo ( URL_REWRITE ) ? 
							SITE_ROOT . 'logout' 
						  : SITE_ROOT . '?page=logout'; ?>">
						  Logout</a>
			</li>

			<?php else: ?>

			<li class="nav-item">
				<a class="nav-link" href="sign-up.php">Sign Up</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="login.php">Login</a>
			</li>

			<?php endif; ?>

			<li class="nav-item">
				<a class="nav-link disabled" href="#"><?php echo $_SESSION['email']; ?></a>
			</li>

		</ul>

</header>	
		

