<!--        FOOTER-->
<footer id="footer-container" class="container-fluid">
<div id="footer-row" class="row">
	
	<div class="col-sm-4">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">About Us</h5>
				<p class="card-text">All about our Company</p>
				<ul class="list-group">
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				</ul>
			
				
			</div>
		</div>
	</div>

	<div class="col-sm-4">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Store Owner</h5>
				<p class="card-text">All about maintaining your store</p>
				
				<ul class="list-group">
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
				  <li class="list-group-item"><a href="#" class="btn btn-primary">Go somewhere</a></li>
					
				</ul>
			</div>
		</div>
	</div>
	

	<div class="col-sm-4">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Social</h5>
				<p class="card-text">See our Social Sites </p>
				<ul class="list-group">
					<li class="list-group-item"><a href="#"><i class="fas fa-camera-retro fa-lg"></i>&nbsp;&nbsp;&nbsp;Instagram</a></li>
					
				  	<li class="list-group-item"><a href="#"><i class="fab fa-facebook-square fa-lg"></i>&nbsp;&nbsp;&nbsp;Facebook</a></li>
					
				  	<li class="list-group-item"><a href="#"><i class="fab fa-twitter-square fa-lg"></i>&nbsp;&nbsp;&nbsp;Twitter</a></li>
					
				  	<li class="list-group-item"><a href="#"><i class="fab fa-google-plus-square fa-lg"></i>&nbsp;&nbsp;&nbsp;Google</a></li>
									
				  	<li class="list-group-item"><a href="#"><i class="fab fa-pinterest-square fa-lg"></i>&nbsp;&nbsp;&nbsp;Pinterest</a></li>
					
					<li class="list-group-item"><i class="fab fa-youtube-square fa-lg"></i>&nbsp;&nbsp;&nbsp;<a href="#">Youtube</a></li>
				</ul>				
			</div>
		</div>
	</div>

</div>
</footer>

		<!--BOOTSRAP SCRIPT CONNECT -->
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>	