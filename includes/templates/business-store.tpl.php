<?php echo 'session type = ' . $_SESSION['type']; ?>

<?php include('includes/templates/components/head.tpl.php'); ?>
<?php include('includes/templates/components/nav.tpl.php'); ?>

        <img src="<?php echo $store_folder . $row[ 'banner_filename' ]; ?>" alt="<?php echo $row[ 'store_name' ]; ?>" />


        <?php while( $row = mysqli_fetch_assoc($result) ): ?>

            <div class="design-display col12">

                <ul>

                    <li><img src="<?php echo $design_folder . $row[ 'file_name' ]; ?>" alt="<?php echo $row[ 'name' ]; ?>" /></li>

                    <li>Product ID =
                        <?php echo $row[ 'product_id' ]; ?>
                    </li>

                    <li>Product NAME =
                        <?php echo $row[ 'name' ]; ?>
                    </li>

                    <li>Product DESCRIPTION =
                        <?php echo $row[ 'description' ]; ?>
                    </li>

                    <li>STORE ID =
                        <?php echo $row[ 'store_id' ]; ?>
                    </li>

                    <?php if( $row[ 'type' ] ==  1 ): ?>
                        <li>
                            <a class="genericons-neue genericons-neue-close-alt" href="?action=delete&amp;delete_id=<?php echo $row[ 'product_id' ]; ?>">
                                <span>Delete</span>
                            </a>
                        </li>
                        <?php endif; ?>

                </ul>
            </div>

            <?php endwhile; ?>


                <!--            -------------------------------------------------------------   -->


                <!--   UPLOADER / MY CART CONTAINER -->
                <section class="uploader col13 fr">

                    <div class="col33 upload-store">

                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">

                            <?php echo $errors[ 'storeupload' ]; ?>

                                <input type="hidden" name="store-upload-started" value="true" />

                                <ol>

                                    <li>

                                        <label>Store Status</label>

                                        <?php echo $errors[ 'promotion' ]; ?>
                                            <textarea name="promotion" rows="3" cols="80">
                                                <?php echo $_POST[ 'promotion' ]; ?>
                                            </textarea>
                                    </li>

                                    <li>
                                        <label>Change Banner</label>

                                        <input type="file" name="store-user-upload" />

                                    </li>

                                    <li>

                                        <input type="submit" value="Upload" />

                                    </li>

                                </ol>

                        </form>

                    </div>






                    <div class="col33 upload-design">

                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                            <?php echo $errors[ 'upload' ]; ?>
                                <input type="hidden" name="upload-started" value="true" />
                                <ol>
                                    <li>
                                        <label>Product Name</label>
                                        <?php echo $errors[ 'name' ]; ?>
                                            <input type="text" name="name" size="80" value="<?php echo $_POST[ 'name' ]; ?>" />
                                    </li>

                                    <li>
                                        <label>Product Description</label>
                                        <textarea name="description" rows="3" cols="80">
                                            <?php 
						echo $_POST[ 'description' ]; ?>
                                        </textarea>
                                    </li>
                                    <li>
                                        <input type="file" name="user-upload" />
                                    </li>
                                    <li>
                                        <input type="submit" value="Upload" />
                                    </li>
                                </ol>
                        </form>
                    </div>

                    <div>
                        <h2>MY CART</h2></div>

                </section>

                <div class="clear"></div>
                <!--   FOOTER-->
                <?php //include('includes/templates/components/footer.tpl.php'); ?>
</body>

</html>
