<form action="<?php echo $_SERVER[ 'PHP_SELF' ]; ?>" method="post">
    <input type="text" 
           name="password" 
           size="30" 
           placeholder="password" 
           value="<?php echo $_POST[ 'password' ]; ?>" />
    <input type="submit" value="Hash" />
</form>

<?php if( isset( $_POST[ 'password' ] ) ): ?>
<textarea cols="128" rows="1"><?php 
    echo password_hash( $_POST[ 'password' ], PASSWORD_DEFAULT ); 
    ?></textarea>
<?php endif; ?>