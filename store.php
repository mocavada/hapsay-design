<?php
	session_start();


    require( 'includes/config.inc.php' );
    require( 'includes/functions.inc.php' );   

    echo 'STORE ID: ' . $_SESSION[ 'store_id' ];

    //view specific store they clicked in store gallery
    $view_store_id = $_GET['id'];

    //CONNECT TO DB
	$db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME )
        or die( mysqli_connect_error());

    // CONNECT TO UPLOADER FUNCTIONS
	include( 'includes/design-uploader.inc.php' );
	include( 'includes/store-uploader.inc.php' );


//--------------------------------------------------------------------------------//DELETE UPLOAD PRODUCT

switch( $_SERVER[ 'REQUEST_METHOD' ] ){
			
        case 'GET':
			
            switch( $_GET[ 'action' ] ){
                case 'delete':
                    delete_task( $db, $_GET[ 'delete_id' ] );
                break;
					
                default:
                    
                break;
            }
        break;
        default:
            // unsupported request method, this is an error condition
        break;
    }


//--------------------------------------------------------------------------------//GET STORE SELECTED

	$store_query = "SELECT * FROM store 
              WHERE store_id = $view_store_id";

	$store_result = mysqli_query( $db, $store_query )
        or die( mysqli_error( $db ).'<br>'. $store_query );
    
    $store_row = mysqli_fetch_assoc($store_result);


//--------------------------------------------------------------------------------//GET ALL PRODUCTS FOR STORE SELECTED

    $product_query = "SELECT * FROM product
              JOIN store WHERE product.store_id = store.store_id
              AND store.store_id = $view_store_id ORDER BY date_uploaded DESC";

    $product_result = mysqli_query( $db, $product_query )
        or die( mysqli_error( $db ).'<br>'. $product_query );

    //VARIABLES
    $banner_folder  = BANNER_FOLDER;
	$product_folder  = PREVIEW_FOLDER ;

?>
 
<!-- HEAD-->
 <?php include('includes/templates/components/head.tpl.php'); ?>
<!--   NAV-->
<?php include('includes/templates/components/nav.tpl.php'); ?>

<!-- -------------------------------------------------------------------------------- -->
    

    
<!-- -------------------------------------------------------------------------------- -->

<!--    PRODUCT GALLERY-->


<section class="container">
		
		
		<!-- BANNER-->
	<div class="row">
	
    <h4><?php echo $store_row[ 'promotion' ]; ?></h4>
	</div>
	
	<div class="row">
		

		<div class="col-md-8">
			<img class="img-fluid" src="<?php echo $banner_folder . $store_row[ 'banner_filename' ]; ?>" alt="<?php echo $store_row[ 'store_name' ]; ?>" />
		</div>
		
		<div class="col-md-4">
		 <?php
    
            if( $_SESSION[ 'store_id' ] == $store_row[ 'store_id' ] ) {
            
                include( 'includes/templates/components/uploader.tpl.php' );
                
            } else {
    
                echo "<h2>YOU ARE A CUSTOMER</h2>";
    
            }
    
          ?>
		</div>	
		
	 </div>
		
		
		
        <h3>My ProductsF</h3>
        <?php while( $product_row = mysqli_fetch_assoc($product_result) ): ?>

            <div class="design-display col12">

                <ul>
                    <li>
                        <a href='product.php?id=<?php echo $product_row[ 'product_id' ]; ?>'>                    
                            <img src="<?php echo $product_folder . $product_row[ 'file_name' ]; ?>"
                                 alt="<?php echo $product_row[ 'name' ]; ?>" />
                        </a>
                    </li>

                    <li>Product ID =
                        <?php echo $product_row[ 'product_id' ]; ?>
                    </li>

                    <li>Product NAME =
                        <?php echo $product_row[ 'name' ]; ?>
                    </li>

                    <li>Product DESCRIPTION =
                        <?php echo $product_row[ 'description' ]; ?>
                    </li>

                    <li>STORE ID =
                        <?php echo $product_row[ 'store_id' ]; ?>
                    </li>

                    <?php if( $product_row[ 'store_id' ] ==  $_SESSION[ 'store_id' ] ): ?>
                        <li>
                            <a class="genericons-neue genericons-neue-close-alt" href="?action=delete&amp;delete_id=<?php echo $product_row[ 'product_id' ]; ?>&amp;id=<?php echo $_SESSION[ 'store_id' ]; ?>">
                                <span>Delete</span>
                            </a>
                        </li>
                        <?php endif; ?>

                </ul>
            </div>

            <?php endwhile; ?>
        </section>
	
	
	<h2><?php echo $detesucess; ?></h2>
    
<!-- -------------------------------------------------------------------------------- -->

<!--   UPLOADER / MY CART CONTAINER -->
    
      
            <!--   FOOTER-->
            <?php //include('includes/templates/components/footer.tpl.php'); ?>
</body>

</html>
    
