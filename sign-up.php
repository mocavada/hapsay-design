<?php

include('includes/config.inc.php');
include('includes/functions.inc.php');

$page_title = 'Sign-up';


    $db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME )
        or die( mysqli_connect_error() );


    if( $_POST[ 'action' ] == 'sign-up'){
            $errors = sign_up_new_user($db,
                              $_POST[ 'firstname' ],
                              $_POST[ 'lastname' ],
                              $_POST[ 'email' ],
                              $_POST[ 'password' ],
                              $_POST[ 'streetaddress' ],
                              $_POST[ 'city' ],
                              $_POST[ 'province' ],
                              $_POST[ 'postalcode' ],
                              $_POST[ 'phonenumber' ],
                              $_POST[ 'type' ],
                              $_POST[ 'storename' ],
                              $_POST[ 'storedescription' ] );
                                                 

    } else {
        echo 'Nothing Submitted Yet.';
    }


	

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Closet</title>
	
	
	
	 <!-- BOOTSTRAP STYLE LINK -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	
	 <!-- JQUERY LINK -->
    <script src="js/jquery-1.12.4.min.js"></script>
	
    <link rel="stylesheet" href="css/styles.css" />
    
  
    <script>
    
        $(function() {
            
            //HIDE STORE FORM ON LOAD
            $("#store-form").hide();
            
            //CLICK BUSINESS RADIO BUTTON TO SHOW STORE FORM
            $("#exampleRadios2").click(function() {
				
                $("#store-form").show();
            });//business radio button
            
            //CLICK INDIVIDUAL RADIO BUTTON TO HIDE STORE FORM
            $("#exampleRadios1").click(function() {
				
                $("#store-form").hide();
            });//individual radio button
            
        });//docready
    
    </script>

</head>

<body>

     <!--   NAV-->
    <?php include('includes/templates/components/nav.tpl.php'); ?>

	<section id="forms-section" class="container">

		<form id="userform" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">

			<input type="hidden" name="action" value="sign-up" />
			
			
				<div class="form-group">
					<div class="alert alert-warning" role="alert">	
						<?php echo $errors[ 'first_name' ]; ?>
						<?php echo $errors[ 'last_name' ]; ?>
						<?php echo $errors[ 'email' ]; ?>
						<?php echo $errors[ 'password' ]; ?>
						<?php echo $errors[ 'street_address' ]; ?>
						<?php echo $errors[ 'city' ]; ?>
						<?php echo $errors[ 'province' ]; ?>
						<?php echo $errors[ 'postal_code' ]; ?>
						<?php echo $errors[ 'phone_number' ]; ?>
						<?php echo $errors[ 'store_name' ]; ?>
						<?php echo $errors[ 'store_description' ]; ?>
					</div>
					<div class="alert alert-success" role="alert">
						<?php echo $errors[ 'success' ].$signup_next; ?>
					</div>
				</div>	
			
			
				
			
		

			<div class="form-group">

				<legend>Please Enter Your Details</legend>

			</div>	
			
<!--  SELECT USER TYPE -->
			
			<div class="form-check">

				<div class="form-group col-md-6">

					<input class="individual form-check-input" type="radio" id="exampleRadios1" name="type" value="0" checked>

					<label class="form-check-label" for="exampleRadios1">Individual</label>

				</div>

				<div class="form-group col-md-6">

					<input class="business form-check-input"  type="radio" id="exampleRadios2" name="type" value="1">

					<label class="form-check-label" for="exampleRadios2">Business</label>

				</div>
				
			</div>	

<!--  INPUT NAME AND LASTNAME-->
				
			<div class="form-row">
	
 				<div class="form-group col-md-6">
					
					<input class="form-control" id="first-name" placeholder="First name" name="firstname" type="text" size="80" value="<?php echo $_POST[ 'firstname' ]; ?>" />
					
				</div>	
					
				<div class="form-group col-md-6">
					
					<input class="form-control" id="last-name" placeholder="Last name" name="lastname" type="text" size="80" value="<?php echo $_POST[ 'lastname' ]; ?>" />
					
				</div>
				
					
			</div>
			
<!--  INPUT EMAIL AND PASSWORD-->
			
			<div class="form-row">		
				<div class="form-group col-md-6">

					<label for="inputEmail4">Email</label>
					<input class="form-control" placeholder="john.doe@example.com" id="inputEmail4" name="email" type="email" size="80" value="<?php echo $_POST[ 'email' ]; ?>" />

				</div>
				<div class="form-group col-md-6">

					<label for="inputPassword4">Password</label>
					<input class="form-control" id="inputPassword4" placeholder="Enter your password." type="password" name="password" size="80" />
					
				</div>
			</div>
			
			<div class="form-group">
				<legend>Delivery</legend>
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-8">	
					
					<label for="inputAddress">Address</label>
					<input class="form-control" id="inputAddress" placeholder="ex.123 All Street, Unit 1" name="streetaddress" type="text" value="<?php echo $_POST[ 'streetaddress' ]; ?>" />
					
				</div>
				<div class="form-group col-md-4">

					<label for="inputCity">City</label>
					<input class="form-control" id="inputCity" placeholder="ex. Toronto" name="city" type="text" value="<?php echo $_POST[ 'city' ]; ?>" />

				</div>
			</div>	

			<div class="form-row">
				<div class="form-group col-md-4">

					<label for="inputState">Province</label>
					<!-- select: creates a drop-down menu -->
					<select id="inputState" class="form-control" name="province">
					<!-- optgroup: allows categorization of options, with labels. -->
						<optgroup label="Provinces">
							<!-- option: individual options, values are submitted -->
							<option value="AB">Alberta</option>
							<option value="BC">British Columbia</option>
							<option value="MB">Manitoba</option>
							<option value="NB">New Brunswick</option>
							<option value="NL">Newfoundland and Labrador</option>
							<option value="NS">Nova Scotia</option>
							<option selected value="ON">Ontario</option>
							<option value="PE">Prince Edward Island</option>
							<option value="QC">Quebec</option>
							<option value="SK">Saskatchewan</option>
							<option value="NT">Northwest Territories</option>
							<option value="NU">Nunavut</option>
							<option value="YT">Yukon</option>
						</optgroup>
					</select>
				</div> 

				<div class="form-group col-md-4">

					<label for="inputZip">Postal Code</label>
					<input class="form-control" id="inputZip" name="postalcode" type="text" size="6" value="<?php echo $_POST[ 'postalcode' ]; ?>" />

				</div>
				<div class="form-group col-md-4">
					
					<label for="inputNumber">Phone Number</label>
					<input class="form-control" id="inputNumber" name="phonenumber" type="number" size="10" value="<?php echo $_POST[ 'phonenumber' ]; ?>" />
				
				</div>
				
			</div>	

			<!-- STORE FORM-->
			<div id="store-form">
			
				<div class="form-group">
					<legend>Type Your Store Info</legend>
				</div>
				
				<div class="form-group">

					<label for="formGroupExampleInput">Store Name</label>

					<input type="text" class="form-control" id="formGroupExampleInput"
								   name="storename"
								   placeholder="ABC Store"
								   value="<?php echo $_POST[ 'storename' ]; ?>">
				</div>
				
				<div class="form-group">

					<label for="formGroupExampleInput">Describe Your Store</label>
					<input type="text" class="form-control" id="formGroupExampleInput2"
								   name="storedescription"
								   placeholder="Selling Star Wars Designs Shirt"
								   value="<?php echo $_POST[ 'storedescription' ]; ?>">
				</div>
				
			</div>	
			
				<div class="form-group">
					
					<button type="submit" class="btn btn-primary" value="Submit">Sign Up</button>
					
				</div>

		</form>

	</section>

     <?php include('includes/templates/components/footer.tpl.php'); ?>

</body>

</html>
